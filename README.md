# Description

This package allows to simulate the particle fluxes impingin on a satellite in LEO. 

It makes use of third party softwares:

- [predict-2.3.1](http://www.qsl.net/kd2bd/predict.html),
- [AE8/AP8](https://ccmc.gsfc.nasa.gov/pub/modelweb/radiation_belt/radbelt),
- [AACGM_v2](http://thayer.dartmouth.edu/superdarn/aacgm.html).

Sources of the codes have been adapted and are already included in this package.
Please refer to the links above for correctly quote the authors of those packages in
your publications.

#### 1. Orbit Simulation 

Two kinds of orbit, defined as latitude, longitude and altitude as function of time, are available:

1. a simple circular orbit model based on average orbital parameters;
2. an orbit calculated with two-line element set (TLE). 
 
The TLE orbit calculation comes from a modification of <http://www.qsl.net/kd2bd/predict.html>,
while TLE data are extracted from <http://celestrak.com/NORAD/elements/stations.txt> and 
updated seldomly. Right now only TIANGONG-2 and ISS orbits are included.
  
#### 2. Geomagnetic coordinates and rigidity cutoff 
 
Geographic coordinates can be converted in geomagnetic coordinates, a set of 
coordinates referred to the Earth magnetic field dipole approximation. Available
geomagnetic coordinates systems are:

1. __CDGM__: the centred dipole geomagnetic coordinates, are obtained by a rotation
   of the geographic coordinates that moves the pole axis to the best fit dipole 
   axis passing through center of Earth. 
   The dipole axis determination is taken from <http://wdc.kugi.kyoto-u.ac.jp/poles/polesexp.html>,
2. __EDGM__: the eccentric dipole geomagnetic coordinates, are obtained by a translation
   of the CDGM coordinates that moves the center of the magnetic field away from
   the center of the Earth. The coordinates of the center of the magnetic field are 
   obtained as the best fit of a dipole with the same direction of CDGM coordinates.
   The dipole center coord. are taken from <https://www.spenvis.oma.be/help/background/magfield/cd.html)>,
3. __AACGM__: the altitude-adjusted corrected geomagnetic coordinates, are defined such 
   that all points along a magnetic field line have the same geomagnetic latitude and 
   longitude. This coordinates obtained by following field lines are not always 
   defined. 
   The code used to calculate this coordinates has been adapted from the original in
   <http://thayer.dartmouth.edu/superdarn/aacgm.htmli>.

Geomagnetic coordinates can be used to estimate the vertical and directional Stoermer 
rigidity cutoff, that is the minimum rigidity a particle should have to penetrate 
the Earth's magnetic field.

#### 3. Particle Fluxes

Particle fluxes have different sources. Available model are:

1. __GALPROP__: evaluation of galactic cosmic rays fluxes (e+,e-,p,p-bar,and all elements 
   isotopes). These fluxes are subject to solar modulation (Gleeson&Axford FFA approach) 
   and to Earth rigidity cutoff, that can be also included in the calculation.
   The used tables were obtained using official GALPROP v56 code.
2. __AMS01__: measurements of under-cutoff secondary particles produced in the Earth 
   atmosphere by interactions of primaries (AMS Coll., Protons in near earth orbit, Phys. Lett. B 472, 215-226, 2000),
3. __RADBELT__: Van Allen belts trapped particles. Evaluated using the AE-8 and AP-8 models.
   The code have been adapted from <https://ccmc.gsfc.nasa.gov/pub/modelweb/radiation_belt/radbelt>.

New GALPROP simulation, as well as new trapped particles models (AP-9 and AE-9) are available.
Some update is foreseen. 

#### 3. Random Generators

Several random generators for uniform and isotropic distribution, following the expected rates of each species, 
and inter-event time difference are included.

# Installation 

This software depends on [ROOT](https://root.cern.ch). 

```
> source install.sh
```
This will create:
```
install/lib/liborbit.so            # shared library
install/bin/GenerateListOfEvents   # executable for the creation of event lists
install/sbin/setenv.sh             # needed environmental vars
install/include/*h                 # all include files
```

To use libraries and executables the environmental vars must be set:

```
> source install/sbin/setenv.sh
```

In the directory ```macro/``` there are examples of the usage 
of the library:

```
> cd macro
> root -l load.C           # load all examples
> test_orbit()             # orbit simulation
> test_cutoff()            # calculate Stoermer cutoff
> test_exposure()          # calculate exposure time (over cutoff)
> test_mcilwain()          # calculate (L,B/B0)
> test_rates()             # calculate rates for a given orbit
```

# Data 

Package depends on several data file. Here there is a full list of those:

- ```data/aacgm```: [Coefficiencts for AACGM coordinate calculation](http://thayer.dartmouth.edu/superdarn/aacgm.html),

- ```data/ams01```: AMS-01 upward and downward proton measured spectrum in units of (m^2 s sr MeV)^-1 vs GeV, 
  in 8 bins of λ CGM: 0-0.2, 0.2-0.3, 0.3-0.4, 0.4-0.5, 0.5-0.6, 0.7-0.8, 0.8-0.9, 0.9-1.0
  (AMS Coll., Protons in near earth orbit, Phys. Lett. B 472, 215-226, 2000),  

- ```data/ams02```: AMS-02 data.

- ```data/galprop/apj729_be10_1.39My_highres```: Particles LIS fluxes calculated with GALPROP v56 with Trotta et al. 2011 parametrization
  in units of MeV^2.70 × (cm^2 s sr MeV)^-1 vs MeV for p/p-bar/e+/e-, 
  (MeV/n)^2.70 × (cm^2 s sr MeV/n)^-1 vs MeV/n for nuclei with A>1.

- ```data/galprop/old```: Particles LIS fluxes calculated with GALPROP v54 with Trotta et  al. 2011 parametrization
  Units are similar to previous.
  - p-bar, e+, e- from [WebRun](http://galprop.stanford.edu/webrun.php),
  - H, D, He3, He4, and higher-Z nuclei from running alocal installation of ```GALPROP-v54```,

- ```data/galprop/helmod```: New GALPROP tunings, but not available for single elements.

- ```data/phi```: Solar modulation parameter compilation.
  (G.Usoskin et al., Solar modulation parameter for cosmic rays since 1936
  reconstructed from ground based neutron monitors and ionization chambers,
  J. of Geophys. Res. 116, 2011),

- ```data/radbelt```: [AE8 and AP8 radiation belt model tables](https://ccmc.gsfc.nasa.gov/pub/modelweb/radiation_belt/radbelt),

- ```data/tle```: [two line orbit element](http://celestrak.com/NORAD/elements/stations.txt).
