#include "OrbitGenerator.h"

#include "radbelt.h"
#include "aacgmlib_v2.h"
#include "mlt_v2.h"

#include "TRandom.h"
#include "TMath.h"

double OrbitGenerator::amu = 0.931494095; // GeV/c^2
double OrbitGenerator::me = 0.000510998910; // GeV/c^2
double OrbitGenerator::mp = 0.938272081; // GeV/c^2
double OrbitGenerator::R_Earth = 6371.2; // km    // or 6378.137 ?

OrbitGenerator::OrbitGenerator(const char* dir) { 
  map_loglog_flux_LIS_GALPROP.clear();
  map_loglog_flux_GALPROP.clear();
  for (int i=0; i<2; i++) 
    for (int j=0; j<10; j++) 
      loglog_flux_proton_AMS01[i][j] = 0;
  // load
  load_loglog_flux_LIS_GALPROP();
  load_loglog_flux_AMS01();
  load_params_RADBELT();
  // defaults
  Ke_min = 0.010; 
  Kn_min = 0.100; 
  Ke_max = 10000.;
  Kn_max = 10000.; 
  which_models = 0x3;
  only_this_pdgid = -1; 
}

OrbitGenerator::~OrbitGenerator() {
  for (map<int,TGraph*>::iterator it = map_loglog_flux_LIS_GALPROP.begin(); it!=map_loglog_flux_LIS_GALPROP.end(); it++) if (it->second) delete it->second;
  map_loglog_flux_LIS_GALPROP.clear(); 
  for (map<int,TGraph*>::iterator it = map_loglog_flux_GALPROP.begin(); it!=map_loglog_flux_GALPROP.end(); it++) if (it->second) delete it->second;
  map_loglog_flux_GALPROP.clear();
  for (int i=0; i<2; i++) {
    for (int j=0; j<10; j++) {
      if (loglog_flux_proton_AMS01[i][j]) delete loglog_flux_proton_AMS01[i][j]; 
      loglog_flux_proton_AMS01[i][j] = 0;
    }
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int OrbitGenerator::get_PDGID(int Z, int A) {
  int pdgid = 0;
  if      ( (abs(Z)==1)&&(A==1) ) pdgid = 2212; // p/p-bar
  else if (A>1) pdgid = 1000000000 + fabs(Z)*10000 + A*10; // nuclei/anti-nuclei 
  else if (A==0) pdgid = -11; // positron/electron 
  return (Z<0) ? -pdgid : pdgid;
}

int OrbitGenerator::get_Z(int pdgid) {
  int sign = pdgid/abs(pdgid);
  int Z = 0;
  if (abs(pdgid)==2212) Z = sign;
  if (abs(pdgid)>1000000000) Z = sign*((abs(pdgid)/10000)%1000);
  return Z;
}

int OrbitGenerator::get_A(int pdgid) {
  int A = 0;
  if (abs(pdgid)==2212) A = 1;
  else if (abs(pdgid)>1000000000) A = (abs(pdgid)/10)%1000;
  return A;
}

double OrbitGenerator::get_R(double K, int A, int Z) {
  double M = A*amu;
  if (A==0) M = me;
  if (A==1) M = mp;
  return (A==0) ? sqrt(pow(K+M,2)-pow(M,2)) : sqrt(pow(K*A+M,2)-pow(M,2))/fabs(Z);
}

double OrbitGenerator::get_K(double R, int A, int Z) { 
  double M = A*amu;
  if (A==0) M = me;
  if (A==1) M = mp;
  return (A==0) ? sqrt(pow(R,2)+pow(M,2))-M : (sqrt(pow(R*Z,2)+pow(M,2))-M)/A;
}

void OrbitGenerator::get_simple_orbit(double R, double inclination, double period, unsigned int time, double& lat, double& lon, double& alt) {
  // circular orbit on XY (equatorial), rotated around Y
  double omega = 2*M_PI/period;
  double x =  R*sin(omega*time)*cos(inclination);
  double y =  R*cos(omega*time);
  double z = -R*sin(omega*time)*sin(inclination);
  // earth rotation (sidereal day)
  double omega_day = 2*M_PI/(23.9436*3600);
  double xt = x*cos(omega_day*time)-y*sin(omega_day*time);
  double yt = x*sin(omega_day*time)+y*cos(omega_day*time);
  double zt = z;
  lat = asin(zt/R);
  lon = atan2(yt,xt);
  alt = R-R_Earth;
}

void OrbitGenerator::get_TIANGONG2_simple_orbit(unsigned int time, double& lat, double& lon, double& alt) {
  double perigee = 369.65; // km 
  double apogee  = 378.40; // km 
  double inclination = M_PI*42.79/180.; // rad
  double period = 92*60; // s
  double R = 0.5*(apogee+perigee)+R_Earth; // km
  get_simple_orbit(R,inclination,period,time,lat,lon,alt);
}

void OrbitGenerator::get_ISS_simple_orbit(unsigned int time, double& lat, double& lon, double& alt) {
  double perigee = 408; // km 
  double apogee  = 410; // km 
  double inclination = M_PI*51.64/180.; // rad
  double period = 92.68*60; // s
  double R = 0.5*(apogee+perigee)+R_Earth; // km
  get_simple_orbit(R,inclination,period,time,lat,lon,alt);
}

void OrbitGenerator::get_predict_orbit(const char* name, const char* filename, unsigned int time, double& lat, double& lon, double& alt) {
  predict.LoadTLE(name,filename);
  float r,theta,phi,v,vtheta,vphi,grmedphi;
  predict.GTOD(&r,&theta,&phi,&v,&vtheta,&vphi,&grmedphi,double(time));
  alt = r/100000-R_Earth;
  lat = theta;
  lon = phi;
  if (lon>M_PI) lon -= 2*M_PI;
}

void OrbitGenerator::get_TIANGONG2_predict_orbit(unsigned int time, double& lat, double& lon, double& alt) {
  get_predict_orbit("TIANGONG-2",Form("%s/tle/stations_2018_11_19.txt",getenv("ORBIT_GENERATOR_DATA")),time,lat,lon,alt);
}

void OrbitGenerator::get_ISS_predict_orbit(unsigned int time, double& lat, double& lon, double& alt) {
  get_predict_orbit("ISS (ZARYA)",Form("%s/tle/stations_2019_11_19.txt",getenv("ORBIT_GENERATOR_DATA")),time,lat,lon,alt);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// geomagnetic poles dependence on time (IGRF-12)
// http://wdc.kugi.kyoto-u.ac.jp/poles/polesexp.html
static double _poles_wandering[33][10] = { 
  //         GM North      GM South        North         South       Dipole Mom.
  // Year | lat. | lon. | lat. | lon. | lat. | lon. | lat. | lon. | (10^22 Am^2)
  {1900,78.7,-68.8,-78.7,111.2,70.5, -96.2,-71.7,148.3,8.32},
  {1905,78.7,-68.7,-78.7,111.3,70.7, -96.5,-71.5,148.5,8.30},
  {1910,78.7,-68.7,-78.7,111.3,70.8, -96.7,-71.2,148.6,8.27},
  {1915,78.6,-68.6,-78.6,111.4,71.0, -97.0,-70.8,148.5,8.24},
  {1920,78.6,-68.4,-78.6,111.6,71.3, -97.4,-70.4,148.2,8.20},
  {1925,78.6,-68.3,-78.6,111.7,71.8, -98.0,-70.0,147.6,8.16},
  {1930,78.6,-68.3,-78.6,111.7,72.3, -98.7,-69.5,146.8,8.13},
  {1935,78.6,-68.4,-78.6,111.6,72.8, -99.3,-69.1,145.8,8.11},
  {1940,78.5,-68.5,-78.5,111.5,73.3, -99.9,-68.6,144.6,8.09},
  {1945,78.5,-68.5,-78.5,111.5,73.9,-100.2,-68.2,144.4,8.08},
  {1950,78.5,-68.8,-78.5,111.2,74.6,-100.9,-67.9,143.5,8.06},
  {1955,78.5,-69.2,-78.5,110.8,75.2,-101.4,-67.2,141.5,8.05},
  {1960,78.6,-69.5,-78.6,110.5,75.3,-101.0,-66.7,140.2,8.03},
  {1965,78.6,-69.9,-78.6,110.1,75.6,-101.3,-66.3,139.5,8.00},
  {1970,78.7,-70.2,-78.7,109.8,75.9,-101.0,-66.0,139.4,7.97},
  {1975,78.8,-70.5,-78.8,109.5,76.2,-100.6,-65.7,139.5,7.94},
  {1980,78.9,-70.8,-78.9,109.2,76.9,-101.7,-65.4,139.3,7.91},
  {1985,79.0,-70.9,-79.0,109.1,77.4,-102.6,-65.1,139.2,7.87},
  {1990,79.2,-71.1,-79.2,108.9,78.1,-103.7,-64.9,138.9,7.84},
  {1995,79.4,-71.4,-79.4,108.6,79.0,-105.3,-64.8,138.7,7.81},
  {2000,79.6,-71.6,-79.6,108.4,81.0,-109.6,-64.7,138.3,7.79},
  {2005,79.8,-71.8,-79.8,108.2,83.2,-118.2,-64.5,137.8,7.77},
  {2010,80.1,-72.2,-80.1,107.8,85.0,-132.8,-64.4,137.3,7.75},
  {2011,80.1,-72.3,-80.1,107.7,85.4,-137.4,-64.4,137.2,7.74},
  {2012,80.2,-72.4,-80.2,107.6,85.7,-142.5,-64.4,137.0,7.74},
  {2013,80.3,-72.5,-80.3,107.5,85.9,-148.0,-64.3,136.9,7.73},
  {2014,80.3,-72.5,-80.3,107.5,86.1,-153.9,-64.3,136.7,7.73},
  {2015,80.4,-72.6,-80.4,107.4,86.3,-160.0,-64.3,136.6,7.72},
  {2016,80.4,-72.7,-80.4,107.3,86.4,-166.3,-64.2,136.4,7.72},
  {2017,80.5,-72.8,-80.5,107.2,86.5,-172.6,-64.2,136.3,7.72},
  {2018,80.5,-73.0,-80.5,107.0,86.5,-178.8,-64.2,136.1,7.71},
  {2019,80.6,-73.1,-80.6,106.9,86.4,-175.3,-64.1,135.9,7.71},
  {2020,80.6,-73.2,-80.6,106.8,86.4,-169.8,-64.1,135.8,7.70}
};
static TGraph* _lat_geomag_north = 0;
static TGraph* _lon_geomag_north = 0;
int OrbitGenerator::get_CDGM(double lat, double lon, double alt, double& mlat, double& mlon, double& mr, unsigned int time) {
  // init
  int year = (time==0) ? 2015 : int(1970+time/(365*24*3600));
  if (!_lat_geomag_north) { _lat_geomag_north = new TGraph(); for (int i=0; i<33; i++) _lat_geomag_north->SetPoint(i,_poles_wandering[i][0],_poles_wandering[i][1]*M_PI/180); } 
  if (!_lon_geomag_north) { _lon_geomag_north = new TGraph(); for (int i=0; i<33; i++) _lon_geomag_north->SetPoint(i,_poles_wandering[i][0],_poles_wandering[i][2]*M_PI/180); } 
  double lat_geomag_north = _lat_geomag_north->Eval(year);
  double lon_geomag_north = _lon_geomag_north->Eval(year);
  // coordinates
  double x = cos(lat)*cos(lon);
  double y = cos(lat)*sin(lon);
  double z = sin(lat);
  // rotation to magnetic pole meridian
  double angle = lon_geomag_north;
  double x1 =  x*cos(angle)+y*sin(angle);
  double y1 = -x*sin(angle)+y*cos(angle);
  double z1 =  z;
  // rotation of the pole 
  angle = M_PI/2-lat_geomag_north;
  double x2 = x1*cos(angle)-z1*sin(angle);
  double y2 = y1;
  double z2 = x1*sin(angle)+z1*cos(angle);
  mlat = asin(z2);
  mlon = atan2(y2,x2);
  mr   = (alt+R_Earth)/R_Earth; 
  return 0;
}

// position of eccentric dipole
// from https://www.spenvis.oma.be/help/background/magfield/cd.html
double dipole_center[5][12] = {
  {   1945,   1950,   1955,   1960,   1965,   1970,   1975,   1980,   1985,   1990,   1995,   2000},
  {-355.24,-359.03,-362.59,-365.90,-368.77,-373.13,-378.57,-385.41,-391.78,-396.49,-400.51,-401.86},
  { 175.47, 190.67, 203.52, 214.78, 223.78, 230.96, 237.02, 247.49, 258.51, 270.82, 282.84, 300.25},	
  {  92.33, 101.29, 110.75, 122.42, 133.56, 146.40, 159.83, 170.21, 178.73, 185.88, 192.87, 200.61},
  { 406.83, 418.95, 430.30, 441.58, 451.57, 462.60, 474.38, 488.63, 502.26, 514.87, 526.89, 540.27}
};
static TGraph* _x_dipole_center = 0;
static TGraph* _y_dipole_center = 0;
static TGraph* _z_dipole_center = 0;
int OrbitGenerator::get_EDGM(double lat, double lon, double alt, double& mlat, double& mlon, double& mr, unsigned int time) {
  // init
  int year = (time==0) ? 2015 : int(1970+time/(365*24*3600));
  if (!_x_dipole_center) _x_dipole_center = new TGraph(12,dipole_center[0],dipole_center[1]); 
  if (!_y_dipole_center) _y_dipole_center = new TGraph(12,dipole_center[0],dipole_center[2]);    
  if (!_z_dipole_center) _z_dipole_center = new TGraph(12,dipole_center[0],dipole_center[3]);    
  double xC = _x_dipole_center->Eval(year);
  double yC = _y_dipole_center->Eval(year);
  double zC = _z_dipole_center->Eval(year);
  double delta = sqrt(xC*xC+yC*yC+zC*zC);
  // centred dipole + shift
  int ret = get_CDGM(lat,lon,alt,mlat,mlon,mr,time);
  double x = R_Earth*mr*cos(mlat)*cos(mlon)-xC;
  double y = R_Earth*mr*cos(mlat)*sin(mlon)-yC;
  double z = R_Earth*mr*sin(mlat)          -zC;
  mr = sqrt(x*x+y*y+z*z)/R_Earth;
  x /= mr*R_Earth;
  y /= mr*R_Earth;
  z /= mr*R_Earth;
  mlat = asin(z);
  mlon = atan2(y,x);
  return ret; 
}

int OrbitGenerator::get_AACGM(double lat, double lon, double alt, double& mlat, double& mlon, double& mr, unsigned int time) {
  mlat = 0;
  mlon = 0;
  mr = 0;
  int ret = 0;
  if      (time==0) AACGM_v2_SetDateTime(2015,7,1,0,0,0); 
  else if (time< 0) AACGM_v2_SetNow(); 
  else { 
    time_t t = time;
    struct tm* ptm = gmtime(&t);
    AACGM_v2_SetDateTime(ptm->tm_year+1900,ptm->tm_mon,ptm->tm_mday,ptm->tm_hour,ptm->tm_min,ptm->tm_sec);
  }
  double k = 180/M_PI;   
  int err = AACGM_v2_Convert(lat*k,lon*k,alt,&mlat,&mlon,&mr,G2A);
  if (err<0) {
    err = AACGM_v2_Convert(lat*k,lon*k,alt,&mlat,&mlon,&mr,G2A|TRACE);
    ret = 1;
  }
  if (err<0) { 
    double epsilon = 0.01;
    double lat1 = -0.008-epsilon;
    double lat2 =  0.361+epsilon;
    double lon1 = -0.674-epsilon;
    double lon2 =  0.196+epsilon;
    double x = (lon-lon1)/(lon2-lon1);
    double y = (lat-lat1)/(lat2-lat1);
    double w[4] = {(1-x)*(1-y),x*(1-y),(1-x)*y,x*y}; // w
    int _ret[4];
    double _mlat[4],_mlon[4],_mr[4];
    double k = 180/M_PI;
    _ret[0] = AACGM_v2_Convert(k*lat1,k*lon1,alt,&_mlat[0],&_mlon[0],&_mr[0],time); // f(0,0)
    _ret[1] = AACGM_v2_Convert(k*lat1,k*lon2,alt,&_mlat[1],&_mlon[1],&_mr[1],time); // f(1,0)
    _ret[2] = AACGM_v2_Convert(k*lat2,k*lon1,alt,&_mlat[2],&_mlon[2],&_mr[2],time); // f(0,1)
    _ret[3] = AACGM_v2_Convert(k*lat2,k*lon2,alt,&_mlat[3],&_mlon[3],&_mr[3],time); // f(1,1)
    if ( (_ret[0]<0)||(_ret[1]<0)||(_ret[2]<0)||(_ret[3]<0) ) err = -1;
    mlat = 0; for (int i=0; i<4; i++) mlat += w[i]*_mlat[i];
    mlon = 0; for (int i=0; i<4; i++) mlon += w[i]*_mlon[i];
    mr   = 0; for (int i=0; i<4; i++) mr   += w[i]*_mr[i];
    ret = 2;
  }
  mlat /= k;
  mlon /= k;
  if (err<0) {
    get_EDGM(lat,lon,alt,mlat,mlon,mr,time);
    ret = 3;
  }
  return ret;
}

double OrbitGenerator::get_Stoermer_cutoff(double mr, double mlat, double mlon, double* dir) {
  double M = 58; // from Smart and Shea, Adv. Spa. Res. 36 (2005), instead of the old 59.6
  if (dir==0) return M*pow(mr,-2.)*pow(cos(mlat),4.)/4;
  double k[3] = {cos(mlat)*cos(mlon),cos(mlat)*sin(mlon),sin(mlat)}; // zenith axis
  double i[3] = {k[1]/sqrt(k[1]*k[1]+k[0]*k[0]),-k[0]/sqrt(k[1]*k[1]+k[0]*k[0]),0}; // east-west axis
  double j[3] = { // south direction axis
    k[1]*i[2]-k[2]*i[1],
    k[2]*i[0]-k[0]*i[2],
    k[0]*i[1]-k[1]*i[0]   
  };   
  double epsilon = acos(dir[0]*k[0]+dir[1]*k[1]+dir[2]*k[2]);
  double xi = atan2(dir[0]*j[0]+dir[1]*j[1]+dir[2]*j[2],dir[0]*i[0]+dir[1]*i[1]+dir[2]*i[2]);
  return M*pow(mr,-2.)*pow(cos(mlat),4.)*pow(1+sqrt(1-sin(epsilon)*sin(xi+M_PI/2)*pow(cos(mlat),3.)),-2.); 
}

double OrbitGenerator::get_maximum_Stoermer_cutoff(double mr, double mlat, double mlon, double theta_min, double theta_max) {
  double maximum = 0;
  for (int iphi=0; iphi<=10; iphi++) {
    double phi = 2*M_PI*iphi/10.;
    for (int itheta=0; itheta<=10; itheta++) {
      double theta = theta_min+(theta_max-theta_min)*itheta/10.;
      double dir[3] = {sin(theta)*cos(phi),sin(theta)*sin(phi),cos(theta)};
      double cutoff = get_Stoermer_cutoff(mr,mlat,mlon,dir);
      if (cutoff>maximum) maximum = cutoff;
    }
  }
  return maximum; 
}

double OrbitGenerator::get_minimum_Stoermer_cutoff(double mr, double mlat, double mlon, double theta_min, double theta_max) {
  double minimum = 1000;
  for (int iphi=0; iphi<=10; iphi++) {
    double phi = 2*M_PI*iphi/10.;
    for (int itheta=0; itheta<=10; itheta++) {
      double theta = theta_min+(theta_max-theta_min)*itheta/10.;
      double dir[3] = {sin(theta)*cos(phi),sin(theta)*sin(phi),cos(theta)};
      double cutoff = get_Stoermer_cutoff(mr,mlat,mlon,dir);
      if (cutoff<minimum) minimum = cutoff;
    }
  }
  return minimum;
}

double OrbitGenerator::get_LShell(double mr, double mlat) {
  return mr*pow(cos(mlat),-2);
}

double OrbitGenerator::get_B_on_B0(double mr, double mlat) {
  double l = get_LShell(mr,mlat); 
  return pow(l/mr,3.)*sqrt(1+3*pow(sin(mlat),2));
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void OrbitGenerator::load_loglog_flux_LIS_GALPROP(const char* dir) {
  // clean-up
  for (map<int,TGraph*>::iterator it = map_loglog_flux_LIS_GALPROP.begin(); it!=map_loglog_flux_LIS_GALPROP.end(); it++) if (it->second) delete it->second;
  map_loglog_flux_LIS_GALPROP.clear();
  // load
  for (int Z=-1; Z<=28; Z++) {
    if (Z==0) continue;
    for (int A=abs(Z)-1; A<=3*abs(Z); A++) {
      FILE* file = fopen(Form("%s/galprop/apj729_be10_1.39My_highres/Z%02d_A%02d.txt",dir,Z,A),"r");
      if (!file) continue;
      TGraph* loglog_flux = new TGraph();
      loglog_flux->SetName(Form("loglog_flux_LIS_GALPROP_Z%02d_A%02d",Z,A));
      float e,f;
      while (!feof(file)) {
        int ret = fscanf(file,"%f%f",&e,&f);
        if (ret==2) {
          double k_lis = e*1e-3; // GeV/n 
          double f_k_lis = f*pow(e,-2.7)*1e7; // (GeV/n m^2 sr s)^{-1}
          loglog_flux->SetPoint(loglog_flux->GetN(),log10(k_lis),log10(TMath::Max(f_k_lis,1e-100)));
        }
      }
      fclose(file);
      if (!loglog_flux) continue;
      int pdgid = get_PDGID(Z,A);
      map_loglog_flux_LIS_GALPROP[pdgid] = loglog_flux; 
    }
  }
}

void OrbitGenerator::prepare_loglog_flux_GALPROP(double phi) {
  static double _phi_GALPROP = -1;
  if (fabs(_phi_GALPROP-phi)<1e-5) return;
  // clean-up
  for (map<int,TGraph*>::iterator it = map_loglog_flux_GALPROP.begin(); it!=map_loglog_flux_GALPROP.end(); it++) if (it->second) delete it->second;
  map_loglog_flux_GALPROP.clear();
  // apply force-field
  for (map<int,TGraph*>::iterator it = map_loglog_flux_LIS_GALPROP.begin(); it!=map_loglog_flux_LIS_GALPROP.end(); it++) {
    int pdgid = it->first;
    int Z = get_Z(pdgid);
    int A = get_A(pdgid);
    TGraph* loglog_flux = new TGraph();
    loglog_flux->SetName(Form("loglog_flux_GALPROP_Z%02d_A%02d",Z,A));
    for (int i=0; i<it->second->GetN(); i++) {
      double k_lis, f_k_lis;
      it->second->GetPoint(i,k_lis,f_k_lis);
      k_lis = pow(10,k_lis); 
      f_k_lis = pow(10,f_k_lis);  
      double k = (A!=0) ? k_lis-fabs(Z)*phi/A : k_lis-phi;
      if (k>0) {
        double m = amu;
        if (A==0) m = me;
        if (A==1) m = mp;
        double f_k = f_k_lis*(k*(k+2*m))/(k_lis*(k_lis+2*m));
        double r = (A!=0) ? (1.*A/abs(Z))*sqrt(k*(k+2*m)) : sqrt(k*(k+2*m)); // GV
        double f_r = (A!=0) ? f_k*pow(1.*fabs(Z)/A,2.)*r/(k+m) : f_k*r/(k+m); // (GV m^2 sr s)^{-1}
        loglog_flux->SetPoint(loglog_flux->GetN(),log10(r),log10(TMath::Max(f_r,1e-100)));
      }
      int pdgid = it->first;
      map_loglog_flux_GALPROP[pdgid] = loglog_flux;
    }
  }
  _phi_GALPROP = phi;
}

TGraph* OrbitGenerator::get_loglog_flux_GALPROP(int pdgid, double phi) {
  prepare_loglog_flux_GALPROP(phi);
  if (map_loglog_flux_GALPROP.find(pdgid)==map_loglog_flux_GALPROP.end()) return 0; 
  return map_loglog_flux_GALPROP[pdgid];
}

double OrbitGenerator::get_flux_GALPROP(int pdgid, double r, double phi) { 
  TGraph* g = get_loglog_flux_GALPROP(pdgid,phi);
  return (g) ? pow(10,g->Eval(log10(r))) : 0; 
}
  
double OrbitGenerator::get_integral_flux_GALPROP(int pdgid, double rmin, double rmax, double phi) { 
  return _get_integral_flux(get_loglog_flux_GALPROP(pdgid,phi),rmin,rmax); 
}

void OrbitGenerator::load_loglog_flux_AMS01(const char* dir) {
  // reset 
  for (int i=0; i<2; i++) {
    for (int j=0; j<10; j++) {
      if (loglog_flux_proton_AMS01[i][j]) delete loglog_flux_proton_AMS01[i][j];  
      loglog_flux_proton_AMS01[i][j] = new TGraph(); 
      loglog_flux_proton_AMS01[i][j]->SetName(Form("loglog_flux_proton_AMS01_K%02d_L%02d",i,j)); 
    }
  }
  // read
  for (int i=0; i<2; i++) {
    FILE* file = 0;
    if (i==0) file = fopen(Form("%s/ams01/ams01_p_upward.txt",dir),"r");
    else      file = fopen(Form("%s/ams01/ams01_p_downward.txt",dir),"r");
    if (!file) continue;
    while (!feof(file)) {
      float k1,k2,v,e,p;
      fscanf(file,"%f%f",&k1,&k2);
      double k = sqrt(k1*k2); // GeV 
      double r = sqrt(k*(k+2*mp));
      for (int j=0; j<10; j++) { 
        int ret = fscanf(file,"%f%f%f",&v,&e,&p); 
        if (ret==3) {
          if (v>0) {
            v = v*1e3*pow(10,p); // (m2 s sr GeV)^-1
            e = e*1e3*pow(10,p); // (m2 s sr GeV)^-1
            double jacobian = r/(k+mp);  
            int index = loglog_flux_proton_AMS01[i][j]->GetN();
            loglog_flux_proton_AMS01[i][j]->SetPoint(index,log10(r),log10(v*jacobian));
            //loglog_flux_proton_AMS01[i][j]->SetPointError(index,0,e/(v*log(10)));
          }
        }
      } 
    }
    fclose(file);
  }
}

TGraph* OrbitGenerator::get_loglog_flux_AMS01(int pdgid, double mlat, bool upward) {
  if (pdgid!=2212) return 0; // assume all p, should be p+d (Z=1) to be correct
  int i = (upward)?0:1;
  int j = fabs(mlat)/0.1; 
  if (j>0) j -= 1;
  if (j>9) j = 9;
  if ( (upward)&&(j>8) ) j = 8;
  return loglog_flux_proton_AMS01[i][j];
}

double OrbitGenerator::get_flux_AMS01(int pdgid, double r, double mlat, bool upward) { 
  TGraph* g = get_loglog_flux_AMS01(pdgid,mlat,upward);
  return (g) ? pow(10,g->Eval(log10(r))) : 0; 
}

double OrbitGenerator::get_integral_flux_AMS01(int pdgid, double rmin, double rmax, double mlat, bool upward) {
  return _get_integral_flux(get_loglog_flux_AMS01(mlat,upward),rmin,rmax);
}

void OrbitGenerator::load_params_RADBELT(const char* dir) {
  RadBelt::populate_arrays(Form("%s/radbelt",dir));
}

double OrbitGenerator::get_flux_RADBELT(int pdgid, double r, double l, double bb0, double phi) {
  double rmin = r*0.5;
  double rmax = r*2;
  double dr = rmax-rmin;
  return get_integral_flux_RADBELT(pdgid,rmin,rmax,l,bb0,phi)/dr;
}
 
double OrbitGenerator::get_integral_flux_RADBELT(int pdgid, double rmin, double rmax, double l, double bb0, double phi) {
  int particle = -1;
  if ( (l<1)||(l>15)||(bb0<1.)||(bb0>1000.) ) return 0.;
  if      (pdgid==11)   particle = 0; // assume all e-, should be e-+e+ to be correct
  else if (pdgid==2212) particle = 1; // assume all p, should be p+d to be correct
  else return 0;
  double kmin = get_K(rmin,pdgid);
  double kmax = get_K(rmax,pdgid);
  if ( (particle==0)&&(kmin<0.04e-3) ) kmin = 0.04e-3;
  if ( (particle==0)&&(kmin>7.00e-3) ) kmin = 7.00e-3;
  if ( (particle==1)&&(kmin<0.10e-3) ) kmin = 0.10e-3;
  if ( (particle==1)&&(kmin>400.e-3) ) kmin = 400.e-3;
  double Ismin = RadBelt::eval_integral_flux(0,particle,kmin*1000,l,bb0)-RadBelt::eval_integral_flux(0,particle,kmax*1000,l,bb0);
  double Ismax = RadBelt::eval_integral_flux(1,particle,kmin*1000,l,bb0)-RadBelt::eval_integral_flux(1,particle,kmax*1000,l,bb0);
  double Isint = Ismax + (Ismin-Ismax)*(phi-0.4)/(1.5-0.4);
  return 1e4*Isint/4*M_PI; // from (cm^2 s)^-1 to (m^2 sr s)^-1 (?)
}

double OrbitGenerator::get_flux(int pdgid, double r, double rc, double mr, double mlat, double phi) {
  double flux = 0;
  if (which_models&0x1) flux += (r<rc) ? 0 : get_flux_GALPROP(pdgid,r,phi); // apply cutoff
  if (which_models&0x2) flux += get_flux_AMS01(pdgid,r,mlat,true);
  if (which_models&0x4) flux += get_flux_RADBELT(pdgid,r,get_LShell(mr,mlat),get_B_on_B0(mr,mlat),phi);
  return flux;
}

double OrbitGenerator::get_integral_flux(int pdgid, double rmin, double rmax, double rc, double mr, double mlat, double phi) { 
  double integral = 0;
  if (which_models&0x1) integral += (rmax<rc) ? 0 : get_integral_flux_GALPROP(pdgid,(rc>rmin)?rc:rmin,rmax,phi); // apply cutoff 
  if (which_models&0x2) integral += get_integral_flux_AMS01(pdgid,rmin,TMath::Min(100.,rmax),mlat,true);
  if (which_models&0x4) integral += get_integral_flux_RADBELT(pdgid,rmin,rmax,get_LShell(mr,mlat),get_B_on_B0(mr,mlat),phi);
  return integral; 
}

double OrbitGenerator::get_total_rate(double acceptance, double rc, double mr, double mlat, double phi) {
  double total_rate = 0;
  prepare_loglog_flux_GALPROP(phi);      
  for (map<int,TGraph*>::iterator it = map_loglog_flux_LIS_GALPROP.begin(); it!=map_loglog_flux_LIS_GALPROP.end(); it++) {
    int pdgid = it->first;
    if ( (only_this_pdgid>0)&&(only_this_pdgid!=pdgid) ) continue;
    double kmin = (get_A(pdgid)==0) ? Ke_min : Kn_min;
    double kmax = (get_A(pdgid)==0) ? Ke_max : Kn_max;
    double rmin = get_R(kmin,pdgid);
    double rmax = get_R(kmax,pdgid);
    double rate1 = acceptance*get_integral_flux_GALPROP(pdgid,(rc>rmin)?rc:rmin,rmax,phi);
    double rate2 = acceptance*get_integral_flux_AMS01(pdgid,rmin,TMath::Min(100.,rmax),mlat,true);
    double rate3 = acceptance*get_integral_flux_RADBELT(pdgid,kmin,kmax,get_LShell(mr,mlat),get_B_on_B0(mr,mlat),phi);
    double rate  = rate1+rate2+rate3;
    if (which_models&0x1) total_rate += rate1;
    if (which_models&0x2) total_rate += rate2;
    if (which_models&0x4) total_rate += rate3;
  }
  return total_rate; 
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////

double OrbitGenerator::pick_dt(double acceptance, double rc, double mr, double mlat, double phi, double tau, int prescaling) { 
  double lambda = get_total_rate(acceptance,rc,mr,mlat,phi)/prescaling; 
  double dt = tau - log(gRandom->Rndm())/lambda;
  return dt;
}

int OrbitGenerator::pick_flux_PDGID(double rc, double mr, double mlat, double phi) {
  if (only_this_pdgid>0) return only_this_pdgid;
  prepare_loglog_flux_GALPROP(phi); 
  int nparticle = (int)map_loglog_flux_GALPROP.size();
  double* integ_list = new double[nparticle];
  int*    pdgid_list = new int[nparticle];
  int index = 0;
  double I = 0;
  for (map<int,TGraph*>::iterator it = map_loglog_flux_GALPROP.begin(); it!=map_loglog_flux_GALPROP.end(); it++) {
    int pdgid = it->first;
    double kmin = (get_A(pdgid)==0) ? Ke_min : Kn_min;
    double kmax = (get_A(pdgid)==0) ? Ke_max : Kn_max;
    double rmin = get_R(kmin,pdgid);
    double rmax = get_R(kmax,pdgid);
    if (which_models&0x1) I += get_integral_flux_GALPROP(pdgid,(rc>rmin)?rc:rmin,rmax,phi);
    if (which_models&0x2) I += get_integral_flux_AMS01(pdgid,rmin,TMath::Min(100.,rmax),mlat,true);
    if (which_models&0x4) I += get_integral_flux_RADBELT(pdgid,kmin,kmax,get_LShell(mr,mlat),get_B_on_B0(mr,mlat),phi);
    pdgid_list[index] = it->first;
    integ_list[index] = I;
    index++;
  }
  double random = I*gRandom->Rndm();
  int id = 0;
  for (int i=0; i<index; i++) if (random<integ_list[i]) { id = pdgid_list[i]; break; }
  delete [] integ_list;
  delete [] pdgid_list;
  return id;
}

double OrbitGenerator::pick_flux_energy(int pdgid, double rc, double mr, double mlat, double phi, int& type) {
  if ( (only_this_pdgid>0)&&(only_this_pdgid!=pdgid) ) return 0;
  prepare_loglog_flux_GALPROP(phi);      
  double kmin = (get_A(pdgid)==0) ? Ke_min : Kn_min;
  double kmax = (get_A(pdgid)==0) ? Ke_max : Kn_max;
  double rmin = get_R(kmin,pdgid);
  double rmax = get_R(kmax,pdgid);
  double I[3] = {0};
  double I1 = (which_models&0x1) ? get_integral_flux_GALPROP(pdgid,(rc>rmin)?rc:rmin,rmax,phi) : 0;
  double I2 = (which_models&0x2) ? get_integral_flux_AMS01(pdgid,rmin,TMath::Min(100.,rmax),mlat,true) : 0;
  double I3 = (which_models&0x4) ? get_integral_flux_RADBELT(pdgid,kmin,kmax,get_LShell(mr,mlat),get_B_on_B0(mr,mlat),phi) : 0;
  double random = (I1+I2+I3)*gRandom->Rndm();
  if      (random<I1)    { type = 0; return _pick_flux_energy(get_loglog_flux_GALPROP(pdgid,phi),(rc>rmin)?rc:rmin,rmax); }
  else if (random<I1+I2) { 
    type = 1; 
    double energy = _pick_flux_energy(get_loglog_flux_AMS01(mlat,true),rmin,TMath::Min(100.,rmax)); 
    return energy; 
  }
  else                   { type = 2; return -2; } // Not yet implemented 
  return -1;
}

double OrbitGenerator::_get_integral_flux(TGraph* loglog_flux, double xmin, double xmax) {
  if (!loglog_flux) return 0; 
  double s = 0;
  int n = loglog_flux->GetN();
  for (int i=0; i<n-1; i++) {
    double logx1,logx2,logy1,logy2;
    loglog_flux->GetPoint(i  ,logx1,logy1);
    loglog_flux->GetPoint(i+1,logx2,logy2);
    if ( (logx2<log10(xmin))||(logx1>log10(xmax)) ) continue;
    if (logx1<log10(xmin)) { logx1 = log10(xmin); logy1 = loglog_flux->Eval(log10(xmin)); }
    if (logx2>log10(xmax)) { logx2 = log10(xmax); logy2 = loglog_flux->Eval(log10(xmax)); }
    if ( (i==  0)&&(log10(xmin)<logx1) ) { logx1 = log10(xmin); logy1 = loglog_flux->Eval(log10(xmin)); }
    if ( (i==n-2)&&(log10(xmax)>logx2) ) { logx2 = log10(xmax); logy2 = loglog_flux->Eval(log10(xmax)); }
    double x1 = pow(10,logx1);
    double x2 = pow(10,logx2);
    double y1 = pow(10,logy1);
    double y2 = pow(10,logy2);
    // log-log trapezoid 
    double a = (logy2-logy1)/(logx2-logx1);
    double I_ll = (a!=-1) ? (y2*x2-y1*x1)/(a+1) : (y2/x2)*logx2-(y1/x1)*logx1;
    s += I_ll;
  }
  if ((log10(xmax)<=loglog_flux->GetX()[0])||(log10(xmin)>=loglog_flux->GetX()[n-1])) {
    double logx1 = log10(xmin);
    double logx2 = log10(xmax); 
    double logy1 = loglog_flux->Eval(log10(xmin)); 
    double logy2 = loglog_flux->Eval(log10(xmax)); 
    double x1 = pow(10,logx1);
    double x2 = pow(10,logx2);
    double y1 = pow(10,logy1);
    double y2 = pow(10,logy2);
    // log-log trapezoid 
    double a = (logy2-logy1)/(logx2-logx1);
    s = (a!=-1) ? (y2*x2-y1*x1)/(a+1) : (y2/x2)*logx2-(y1/x1)*logx1;
  }
  return (s>0) ? s : 0;
}

double OrbitGenerator::_pick_flux_energy(TGraph* loglog_flux, double xmin, double xmax) {
  int n = loglog_flux->GetN();
  double  I = 0;
  double* integral = new double[n+1];
  double* bins     = new double[n+2];
  double* alpha    = new double[n+1];  
  int index = 0; 
  for (int i=0; i<n-1; i++) {
    double logx1,logx2,logy1,logy2; 
    loglog_flux->GetPoint(i  ,logx1,logy1);
    loglog_flux->GetPoint(i+1,logx2,logy2);
    if ( (logx2<log10(xmin))||(logx1>log10(xmax)) ) continue;
    if (logx1<log10(xmin)) { logx1 = log10(xmin); logy1 = loglog_flux->Eval(log10(xmin)); }
    if (logx2>log10(xmax)) { logx2 = log10(xmax); logy2 = loglog_flux->Eval(log10(xmax)); }
    if ( (i==  0)&&(log10(xmin)<logx1) ) { logx1 = log10(xmin); logy1 = loglog_flux->Eval(log10(xmin)); }
    if ( (i==n-2)&&(log10(xmax)>logx2) ) { logx2 = log10(xmax); logy2 = loglog_flux->Eval(log10(xmax)); }
    double x1 = pow(10,logx1);
    double x2 = pow(10,logx2);
    double y1 = pow(10,logy1);
    double y2 = pow(10,logy2);
    // log-log trapezoid 
    double a = (logy2-logy1)/(logx2-logx1);
    double I_ll = (a!=-1) ? (y2*x2-y1*x1)/(a+1) : (y2/x2)*logx2-(y1/x1)*logx1; 
    I += I_ll;  
    integral[index] = I;
    alpha   [index] = a;
    bins    [index] = x1;
    bins    [index+1] = x2;
    index++;
  }
  // extract bin
  double random = I*gRandom->Rndm();
  int ibin = -1; 
  for (int i=0; i<index; i++) if (random<integral[i]) { ibin = i; break; }
  // extract inside the bin 
  random = gRandom->Rndm();
  double x1 = bins[ibin];
  double x2 = bins[ibin+1];
  double a  = alpha[ibin];
  double x  = pow(random*(pow(x2,a+1)-pow(x1,a+1))+pow(x1,a+1),1./(a+1));
  // clean-up
  delete [] integral;
  delete [] bins;
  delete [] alpha;
  return x;
}

double OrbitGenerator::pick_uniformly_and_isotropic_from_a_sphere(double point[3], double dir[3], double r, double theta_min, double theta_max) {
  double phi_r   = 2*M_PI*gRandom->Rndm();
  double theta_r = acos(cos(theta_max)+(cos(theta_min)-cos(theta_max))*gRandom->Rndm());
  double a[3] = {sin(theta_r)*cos(phi_r),sin(theta_r)*sin(phi_r),cos(theta_r)};
  for (int i=0; i<3; i++) point[i] = r*a[i];
  double phi   = 2.*M_PI*gRandom->Rndm();
  double theta = acos(sqrt(gRandom->Rndm()));
  dir[0] = sin(theta)*cos(phi);
  dir[1] = sin(theta)*sin(phi);
  dir[2] = -cos(theta);
  if (theta_r>0) {
    double k = 1./(1+a[2]);
    // http://math.stackexchange.com/questions/180418/calculate-rotation-matrix-to-align-vector-a-to-vector-b-in-3d
    double R[3][3] = {
      {1-k*a[0]*a[0],k*(-a[0]*a[1]),-a[0]},
      {k*(-a[0]*a[1]),1-k*a[1]*a[1],-a[1]},
      {a[0],a[1],1-k*a[0]*a[0]-k*a[1]*a[1]}
    };
    double b[3] = {0};
    for (int i=0; i<3; i++) for (int j=0; j<3; j++) b[i] += dir[j]*R[j][i];
    for (int i=0; i<3; i++) dir[i] = b[i];
  }
  return 2*pow(M_PI*r,2)*(cos(theta_min)-cos(theta_max));
}

double OrbitGenerator::pick_uniformly_and_isotropic_from_a_cube(double point[3], double dir[3], double h, int select_side) {
  int which_side = select_side;
  if (select_side==-1) which_side = floor(gRandom->Rndm()*6);
  point[(which_side  )%3] = h*(-1+2*gRandom->Rndm());
  point[(which_side+1)%3] = h*(-1+2*gRandom->Rndm());
  point[(which_side+2)%3] = (which_side<3) ? h : -h;
  double phi = 2.*M_PI*gRandom->Rndm();
  double theta = acos(sqrt(gRandom->Rndm()));
  dir[(which_side  )%3] = sin(theta)*cos(phi);
  dir[(which_side+1)%3] = sin(theta)*sin(phi);
  dir[(which_side+2)%3] = (which_side<3) ? -cos(theta) : cos(theta);
  double acc_one_side = 4*M_PI*h*h;
  return (select_side==-1) ? 6*acc_one_side : acc_one_side;
}

