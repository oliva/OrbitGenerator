#ifdef __CINT__
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclasses;
#pragma link C++ class OrbitGenerator+;
#pragma link C++ class Predict+;
#pragma link C++ class RadBelt+;
#pragma link C++ class sat_t;
#pragma link C++ class qth_t;
#pragma link C++ class sat_db_t;
#pragma link C++ class tle_t;
#pragma link C++ class geodetic_t;
#pragma link C++ class vector_t;
#pragma link C++ class deep_arg_t;
#pragma link C++ defined_in "aacgmlib_v2.h";
#pragma link C++ defined_in "astalg.h";
#pragma link C++ defined_in "genmag.h";
#pragma link C++ defined_in "igrflib.h";
#pragma link C++ defined_in "mlt_v2.h";
#pragma link C++ defined_in "rtime.h";
#endif
