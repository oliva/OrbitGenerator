#include "OrbitGenerator.h"

#include "TFile.h"
#include "TTree.h"
#include "TMath.h"

#include <cstdio>
#include <iostream>
#include <unistd.h>

using namespace std; 

/// SOME SMART WAY FOR PRESCALING? 

/// PROBLEMS IN EXTRACTION FROM AMS01, 0 rigidities extracted ... 

// main
int main(int argc, char **argv) {

  // settings
  unsigned int start_time      = 1488221530;      // beginning in UTC time (s)
  unsigned int time_interval   = 24*60*60;        // time interval (s)
  int          orbit_type      = 1;               // orbit: 0=simple circular orbit; 1=predict TIANGONG-2 orbit 
  double       phi             = 0.650;           // solar modulation parameter (GV)
  int          gm_coord_type   = 11;              // geomagnetic coordinates: 0=cdgm; 1=edgm; 2=aacgm (>=10 use time, otherwise use 2015)
  int          which_models    = 0x3;             // flux models: 0x1: GALPROP, 0x2: AMS01, 0x4: RADBELT
  int          only_this_pdgid = 2212;            // select a species with PDG ID, use -1 for all
  double       radius          = 0.5;             // generation radius (m)
  double       theta_min       = 0;               // generation min theta (rad)
  double       theta_max       = TMath::Pi();     // generation max theta (rad)
  double       theta_earth     = TMath::Pi()-0.7; // Earth shadowing effect with respect to zenith (rad)
  int          prescaling      = 100;             // prescaling factor

  /*
  int c; 
  while ((c=getopt(argc,argv,"siopgmOrteP"))!=-1) {
    case 's':
      start_time = atoi(optarg);
      break;
    case 'i':
      time_interval = atoi(optarg);
      break;
    case 'o':
      orbit_type = atoi(optarg);
      break;
    case 'p':
      phi = atof(optarg);
      break;
    case 'g':
      gm_coord_type = atoi(optarg);
      break;
    case 'm':
      which_models = atoi(optarg);
      break;
    case 'O':
      only_this_pdgid = atoi(optarg);
      break;
    case 'r':
      radius = atof(optarg);
      break;
    case 't':
      theta_max = atof(optarg);
      break;
    case 'e':
      theta_earth = atof(optarg);
      break;
    case 'P':
      prescaling = atoi(optarg);
      break;
    case '?':
      return 1;
    default:
      abort();
  }
  */

  // init
  OrbitGenerator orbit;
  orbit.which_models    = which_models;
  orbit.only_this_pdgid = only_this_pdgid;
  double acceptance = 2*pow(TMath::Pi()*radius,2)*(cos(theta_min)-cos(theta_max));

  // output
  TFile* file = TFile::Open("test_generate_time.root","recreate");
  TTree* tree = new TTree("orbit","orbit");
  unsigned int time_sec        = 0;   tree->Branch("time_sec"       ,&time_sec       ,"time_sec/i");
  float        time_mus        = 0;   tree->Branch("time_mus"       ,&time_mus       ,"time_mus/F"); 
  float        dt              = 0;   tree->Branch("dt"             ,&dt             ,"dt/F");
  float        latitude        = 0;   tree->Branch("latitude"       ,&latitude       ,"latitude/F");
  float        longitude       = 0;   tree->Branch("longitude"      ,&longitude      ,"longitude/F");
  float        altitude        = 0;   tree->Branch("altitude"       ,&altitude       ,"altitude/F");
  float        gm_latitude     = 0;   tree->Branch("gm_latitude"    ,&gm_latitude    ,"gm_latitude/F");
  float        gm_longitude    = 0;   tree->Branch("gm_longitude"   ,&gm_longitude   ,"gm_longitude/F");
  float        gm_radius       = 0;   tree->Branch("gm_radius"      ,&gm_radius      ,"gm_radius/F");
  float        minimum_cutoff  = 0;   tree->Branch("minimum_cutoff" ,&minimum_cutoff ,"minimum_cutoff/F");
  float        vertical_cutoff = 0;   tree->Branch("vertical_cutoff",&vertical_cutoff,"vertical_cutoff/F");
  float        rate            = 0;   tree->Branch("rate"           ,&rate           ,"rate/F");
  int          pdgid           = 0;   tree->Branch("pdgid"          ,&pdgid          ,"pdgid/I");
  float        position[3]     = {0}; tree->Branch("position"       ,position        ,"position[3]/F");
  float        direction[3]    = {0}; tree->Branch("direction"      ,direction       ,"direction[3]/F");
  int          type            = 0;   tree->Branch("type"           ,&type           ,"type/I");
  float        rigidity        = 0;   tree->Branch("rigidity"       ,&rigidity       ,"rigidity/F");
  float        cutoff          = 0;   tree->Branch("cutoff"         ,&cutoff         ,"cutoff/F");
  tree->Branch("prescaling",&prescaling,"prescaling/I");

  // loop on seconds 
  unsigned int save_time = -1; 
  time_sec = start_time;
  time_mus = 0;
  int ntaken = 0;
  int ngen = 0;
  double mc = 0;
  while (time_sec<start_time+time_interval) {
    // orbit update every second 
    double lat,lon,alt;
    double mlat,mlon,mr;
    if (time_sec!=save_time) {
      // orbit
      if (orbit_type==0) orbit.get_TIANGONG2_simple_orbit(time_sec,lat,lon,alt);
      else               orbit.get_TIANGONG2_predict_orbit(time_sec,lat,lon,alt);
      latitude  = (float)lat;
      longitude = (float)lon;
      altitude  = (float)alt;
      // geomagnetic coordinates 
      if      ((gm_coord_type%10)==2) orbit.get_AACGM(lat,lon,alt,mlat,mlon,mr,(gm_coord_type>=10)?time_sec:0);
      else if ((gm_coord_type%10)==1) orbit.get_EDGM (lat,lon,alt,mlat,mlon,mr,(gm_coord_type>=10)?time_sec:0);
      else                            orbit.get_CDGM (lat,lon,alt,mlat,mlon,mr,(gm_coord_type>=10)?time_sec:0);
      gm_latitude  = (float)mlat;
      gm_longitude = (float)mlon;
      gm_radius    = (float)mr;
      // cutoff estimation 
      mc = orbit.get_minimum_Stoermer_cutoff(mr,mlat,mlon,theta_min,theta_max);
      minimum_cutoff  = (float)mc;
      vertical_cutoff = (float)orbit.get_Stoermer_cutoff(mr,mlat); 
      rate = (float)orbit.get_total_rate(acceptance,mc,mr,mlat,phi)/prescaling;
      save_time = time_sec; 
      cout << "update second: " << time_sec << "    rate: " << rate << " Hz    for acceptance: " << acceptance << " m^2 sr    ntaken: " << ntaken << " of " << ngen << endl;
      ntaken = 0;
      ngen = 0;
    }
    // generate 
    ngen++;
    float dtime = (float)1e6*orbit.pick_dt(acceptance,mc,mr,mlat,phi,0,prescaling);
    dt += dtime;
    time_mus += dtime;
    if (time_mus>1e6) {
      time_sec++;
      time_mus -= 1e6;
    }
    double pos[3] = {0};
    double dir[3] = {0};
    orbit.pick_uniformly_and_isotropic_from_a_sphere(pos,dir,radius,theta_min,theta_max); 
    for (int i=0; i<3; i++) {
      position[i] = (float)pos[i];
      direction[i] = (float)dir[i];
    }
    double theta = acos(dir[2]);
    type  = -1; 
    pdgid = orbit.pick_flux_PDGID(mc,mr,mlat,phi);
    rigidity = (float)orbit.pick_flux_energy(pdgid,mc,mr,mlat,phi,type);
    cutoff   = (float)orbit.get_Stoermer_cutoff(mr,mlat,mlon,dir);
    if ( (type==0)&&(rigidity<cutoff) ) continue; // directional cutoff effect on GCRs 
    if ( (type==0)&&(theta>theta_earth) ) continue; // earth effect on GCRs
    tree->Fill();
    ntaken++;
    dt = 0;
  }
  file->Write();
  file->Close();

  return 1;
}  
