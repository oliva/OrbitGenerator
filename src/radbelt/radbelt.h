#ifndef __RadBelt_h__
#define __RadBelt_h__

#include <sys/types.h>
#include <stdio.h>
#include <math.h>
#include <cstdlib>
#include <cstdio>
#include <iostream>

using namespace std;

class RadBelt { 

 public: 

  static int *MAP, *MAPPRTNS_MIN, *MAPPRTNS_MAX, *MAPELTNS_MIN, *MAPELTNS_MAX; 

  static float FISTEP;
  static int   I1;
  static float FKB1, FKB2, FINCR2, FINCR1;
  static float FKBM, FLOGM, SL2, FNB, DFL;
  static int   J1, J2, ITIME, L1, L2;
  static int   I2, FLOG1, FLOG2;
  static float FKBJ1, FKBJ2, SL1;
  static float FKB, FLOG;

  static void  TRARA1(float FL, float BB0, float *E,float *F,int N, int* DESCR);
  static float TRARA2(int *SUBMAP, float IL,float IB);
  static float trara3(int *SUBMAP, int position);
  static float trara4(int *SUBMAP, int start_psn);
  static float trara5(void);

  RadBelt() {};
  ~RadBelt() {};

  //! Prepare for calculation (solar_modulation=0:min,1:max)
  static void populate_arrays(char* dir);
  //! Evaluate flux of trapped particles (solar_modulation=0:min,1:max particle=0:electrons,1:protons)
  static float eval_integral_flux(int solar_modulation, int particle, float Kmin, float L, float B_on_B0);
  //! zero-ing the static vars 
  static void clear();

};

#endif
