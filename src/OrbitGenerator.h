#ifndef __OrbitGenerator_h__
#define __OrbitGenerator_h__

/**
 * \class OrbitGenerator
 *
 * \author: A.Oliva (alberto.oliva@cern.ch)
 *
 * A class for the calculation of particle fluxes along the orbit.
 * For details see README.md.
 *
 * Some things left to do:
 * - To do: handling of TLE in time.
 * - To do: cutoff from backtracing.
 * - To do: check tables.
 * - To do: smooth interpolation for AMS-01 data? 
 * - To do: electrons from AMS-01? Helium from AMS-01?
 * - To do: include Mizuno et al. model.
 * - To do: gamma rays
 * - To do: better handling of static classes
 */

#include "predict.h"

#include "TCanvas.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TMath.h"
#include "TH1.h"

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h> 
#include <map>

using namespace std;

// static class for orbit and flux estimation
class OrbitGenerator {

 public:

  //! orbit predictor 
  Predict predict; 
  //! map of LIS from GALPROP (key is the PDG ID)
  map<int,TGraph*> map_loglog_flux_LIS_GALPROP;  
  //! map of solar modulated fluxes from GALPROP (key is the PDG ID) 
  map<int,TGraph*> map_loglog_flux_GALPROP;
  //! map of proton upward(0) and downward(1) proton fluxes from AMS-01 (index is \lambda_{CGM})
  TGraph* loglog_flux_proton_AMS01[2][10];

  //! electron/positron minimum kinetic energy (GeV)
  double Ke_min;
  //! electron/positron minimum kinetic energy (GeV)
  double Ke_max;
  //! proton/ions minimum kinetic energy per nucleon (GeV/n)
  double Kn_min; 
  //! proton/ions maximum kinetic energy per nucleon (GeV/n)
  double Kn_max;

  //! combination of used models (0x1: GALPROP, 0x2: AMS01, 0x4: RADBELT)
  int which_models; 
  //! use only this particle kind (-1:all, >0 only particle with that pdg id)
  int only_this_pdgid;     

  //! atomic mass unit [GeV/c^2]
  static double amu;
  //! electron mass [GeV/c^2]
  static double me;
  //! proton mass [GeV/c^2]
  static double mp; 
  //! equatorial radius of the Earth [km] 
  static double R_Earth;
  
  //! c-tor
  OrbitGenerator(const char* dir = getenv("ORBIT_GENERATOR_DATA"));
  //! d-tor
  ~OrbitGenerator(); 

  /////////////////////////////////////
  // PDG functions and conversions 
  /////////////////////////////////////
 
  //! create PDG particle ID (available Z,A pairs: p/p-bar:+/-1,1, nuclei:+Z,A and e+/e-:+/-1,0)  
  int           get_PDGID(int Z, int A);
  //! charge 
  int           get_Z(int pdgid);
  //! mass number (0 for electrons)
  int           get_A(int pdgid);
  //! get rigidity from kinetic energy (per nucleon) [GV]
  double        get_R(double K, int pdgid) { return get_R(K,get_Z(pdgid),get_A(pdgid)); };
  //! get rigidity from kinetic energy (per nucleon) [GV]
  double        get_R(double K, int Z, int A);
  //! get kinetic energy (per nucleon) from rigidity [GeV(/n)]
  double        get_K(double R, int pdgid) { return get_K(R,get_Z(pdgid),get_A(pdgid)); };
  //! get kinetic energy (per nucleon) from rigidity [GeV(/n)]
  double        get_K(double R, int A, int Z);

  ///////////////////////////////////////
  // Orbit
  ///////////////////////////////////////

  //! simple orbit model
  void          get_simple_orbit(double R/*km*/, double inclination/*rad*/, double period/*s*/, unsigned int time/*UT*/, double& lat/*rad*/, double& lon/*rad*/, double& alt/*km*/); 
  //! the orbit model from predict-2.3.1 using TLE in data directory
  void          get_predict_orbit(const char* name, const char* filename, unsigned int time/*UT*/, double& lat/*rad*/, double& lon/*rad*/, double& alt/*km*/);
  
  //! TIANGONG-2 simple orbit model (as it was) 
  void          get_TIANGONG2_simple_orbit(unsigned int time/*UT*/, double& lat/*rad*/, double& lon/*rad*/, double& alt/*km*/);
  //! TIANGONG-2 predicted orbit (as it was)
  void          get_TIANGONG2_predict_orbit(unsigned int time/*UT*/, double& lat/*rad*/, double& lon/*rad*/, double& alt/*km*/);

  //! ISS simple orbit model
  void          get_ISS_simple_orbit(unsigned int time/*UT*/, double& lat/*rad*/, double& lon/*rad*/, double& alt/*km*/);
  //! ISS predicted orbit 
  void          get_ISS_predict_orbit(unsigned int time/*UT*/, double& lat/*rad*/, double& lon/*rad*/, double& alt/*km*/);

  ///////////////////////////////////////
  // Geomagnetic coordinates and cutoff
  ///////////////////////////////////////

  //! from geographic to centred dipole geomagnetic coordinates (definition from https://www.spenvis.oma.be/help/background/magfield/cd.html)  
  int           get_CDGM(double lat/*rad*/, double lon/*rad*/, double alt/*km*/, double& mlat/*rad*/, double& mlon/*rad*/, double& mr, unsigned int time = 0/*UT*/);
  //! from geographic to eccentric dipole geomagnetic coordinates (definition from https://www.spenvis.oma.be/help/background/magfield/cd.html)
  int           get_EDGM(double lat/*rad*/, double lon/*rad*/, double alt/*km*/, double& mlat/*rad*/, double& mlon/*rad*/, double& mr, unsigned int time = 0/*UT*/);
  //! from geographic to geomagnetic coordinates using AACGM_v2 library (0: ok, 1: using tracing; 2: using GM) 
  int           get_AACGM(double lat/*rad*/, double lon/*rad*/, double alt/*km*/, double& mlat/*rad*/, double& mlon/*rad*/, double& mr, unsigned int time = 0/*UT*/);
  //! get directional Stoermer cutoff (vertical if no indication of direction) [GV] 
  double        get_Stoermer_cutoff(double mr, double mlat/*rad*/, double mlon = 0/*rad*/, double* dir = 0);
  //! get maximum Stoermer cutoff in the field of view [GV]
  double        get_maximum_Stoermer_cutoff(double mr, double mlat/*rad*/, double mlon/*rad*/, double theta_min = 0/*rad*/, double theta_max = M_PI/*rad*/);
  //! get minimum Stoermer cutoff in the field of view [GV]
  double        get_minimum_Stoermer_cutoff(double mr, double mlat/*rad*/, double mlon/*rad*/, double theta_min = 0/*rad*/, double theta_max = M_PI/*rad*/);
  //! get L-shell
  double        get_LShell(double mr, double mlat/*rad*/);
  //! get magnetic field value
  double        get_B_on_B0(double mr, double mlat/*rad*/);

  ///////////////////////////////////////
  // Fluxes
  ///////////////////////////////////////

  //! load all available GALPROP fluxes (Trotta param.)
  void          load_loglog_flux_LIS_GALPROP(const char* dir = getenv("ORBIT_GENERATOR_DATA"));
  //! apply solar modulation to all available GALPROP fluxes
  void          prepare_loglog_flux_GALPROP(double phi/*GV*/);
  //! get GALPROP flux [-log10(m^2 sr s GV) vs log10(GV)]
  TGraph*       get_loglog_flux_GALPROP(int pdgid, double phi/*GV*/);
  //! get GALPROP flux [(m^2 sr s GV)^-1]
  double        get_flux_GALPROP(int pdgid, double r/*GV*/, double phi/*GV*/);
  //! get GALPROP integrated flux [(m^2 sr s)^-1]
  double        get_integral_flux_GALPROP(int pdgid, double rmin, double rmax, double phi);

  //! load AMS01 fluxes 
  void          load_loglog_flux_AMS01(const char* dir = getenv("ORBIT_GENERATOR_DATA"));
  //! get AMS01 flux [-log10(m^2 sr s GV) vs log10(GV)]
  TGraph*       get_loglog_flux_AMS01(int pdgid, double mlat/*rad*/, bool upward = true);
  //! get AMS01 flux [(m^2 sr s GV)^-1]
  double        get_flux_AMS01(int pdgid, double r/*GV*/, double mlat/*rad*/, bool upward = true);
  //! get AMS01 integrated flux [(m^2 sr s)^-1]
  double        get_integral_flux_AMS01(int pdgid, double rmin/*GV*/, double xmax/*GV*/, double mlat/*rad*/, bool upward = true);

  //! load parameters of AE-8/AP-8 models
  void          load_params_RADBELT(const char* dir = getenv("ORBIT_GENERATOR_DATA"));
  //! get electron/proton flux from AE-8/AP-8 model [(m^2 sr s GV)^-1]
  double        get_flux_RADBELT(int pdgid, double r/*GV*/, double l, double bb0, double phi/*GV*/); 
  //! get electron/proton flux from AE-8/AP-8 model [(m^2 sr s)^-1]
  double        get_integral_flux_RADBELT(int pdgid, double rmin/*GV*/, double rmax/*GV*/, double l, double bb0, double phi/*GV*/);
  
  //! get flux [(m^2 sr s GV)^-1]
  double        get_flux(int pdgid, double r/*GV*/, double rc/*GV*/, double mr, double mlat, double phi/*GV*/); 
  //! get integrated flux [(m^2 sr s)^-1]
  double        get_integral_flux(int pdgid, double rmin/*GV*/, double rmax/*GV*/, double rc/*GV*/, double mr, double mlat, double phi/*GV*/);

  //! get total rate for a constant, independent from particle, acceptance [Hz]
  double        get_total_rate(double acceptance/*m2 sr*/, double rc/*GV*/, double mr, double mlat, double phi/*GV*/);

  ///////////////////////////////////////
  // Random generators
  ///////////////////////////////////////

  //! pick a dt according to total rate (this is an exponential distribution with cut to deadtime) 
  double        pick_dt(double acceptance/*m^2 sr*/, double rc/*GV*/, double mr, double mlat/*rad*/, double phi/*GV*/, double tau = 0/*s*/, int prescaling = 100); 
  //! pick a particle according to relative abundances (GALPROP+AMS01)
  int           pick_flux_PDGID(double rc/*GV*/, double mr, double mlat/*rad*/, double phi/*GV*/);
  //! pick energy according to flux (type = 0:GALPROP, 1:AMS01, 2: RADBELT) 
  double        pick_flux_energy(int pdgid, double rc/*GV*/, double mr, double mlat/*rad*/, double phi/*GV*/, int& type);
  //! pick energy according to flux 
  double        pick_flux_energy(TGraph* loglog_flux, double rmin/*GV*/, double rmax/*GV*/);
  //! generate uniformly and isotropically inwords from a sphere (returns acceptance value [m^2 sr])
  double        pick_uniformly_and_isotropic_from_a_sphere(double point[3], double dir[3], double r/*m*/, double theta_min = 0/*rad*/, double theta_max = M_PI/*rad*/);
  //! generate uniformly and isotropically inwords from a cube (returns acceptance value, select_side>0: one plane)
  double        pick_uniformly_and_isotropic_from_a_cube(double point[3], double dir[3], double h/*m*/, int select_side = -1);

  /////////////////////////////////////
  // Service
  /////////////////////////////////////

  //! get integrated flux 
  static double _get_integral_flux(TGraph* loglog_flux, double xmin, double xmax);
  //! pick energy according to flux 
  static double _pick_flux_energy(TGraph* loglog_flux, double xmin, double xmax);
};

#endif 
