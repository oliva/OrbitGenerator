#!/bin/sh -x

# a simple script to install the OrbitGenerator library and includes

echo Clean-up ... 
rm -rf install 2>/dev/null
rm -rf src/dict 2>/dev/null

echo Preparing directories install and src/dict ... 
mkdir -p src/dict
mkdir -p install/lib 
mkdir -p install/include
mkdir -p install/bin
mkdir -p install/sbin

echo Preparing ROOT dictionary ... 
rootcint -f src/dict/orbit_generator_dict.cxx -c -I./ -I./src/aacgm -I./src/predict -I./src/radbelt -I$ROOTSYS/include -p src/OrbitGenerator.h \
  src/predict/predict.h src/aacgm/aacgmlib_v2.h src/aacgm/astalg.h src/aacgm/genmag.h src/aacgm/igrflib.h src/aacgm/mlt_v2.h src/aacgm/rtime.h \
  src/radbelt/radbelt.h src/linkdef.h

echo Preparing install/lib/liborbit.so shared library ... 
$CXX -std=c++14 -fPIC -shared -I./ -I./src/aacgm -I./src/predict -I./src/radbelt -I$ROOTSYS/include -o install/lib/liborbit.so src/dict/orbit_generator_dict.cxx \
  src/OrbitGenerator.c src/predict/predict.c src/aacgm/aacgmlib_v2.c src/aacgm/igrflib.c src/aacgm/genmag.c src/aacgm/astalglib.c src/aacgm/mlt_v2.c src/aacgm/rtime.c src/radbelt/radbelt.c

echo Preparing install/bin/GenerateListOfEvents executable ... 
$CXX -std=c++14 -fPIC -I./ -I./src/aacgm -I./src/predict -I./src/radbelt -I$ROOTSYS/include src/GenerateListOfEvents.c `root-config --libs` -L./install/lib -lorbit -o install/bin/GenerateListOfEvents 
chmod 777 install/bin/GenerateListOfEvents 

echo Copying headers in install/include ... 
cp src/OrbitGenerator.h install/include
cp src/aacgm/*.h install/include
cp src/predict/*.h install/include
cp src/radbelt/*.h install/include

SETENV=install/sbin/setenv.sh
echo Create environmental var file $SETENV ...
echo "BASE=$PWD" >> $SETENV
echo "export ORBIT_GENERATOR_DATA=\$BASE/data " >> $SETENV
echo "export AACGM_v2_DAT_PREFIX=\$BASE/data/aacgm/aacgm_coeffs-12- " >> $SETENV
echo "export IGRF_COEFFS=\$BASE/data/aacgm/igrf12coeffs.txt " >> $SETENV
echo "export PATH=\$BASE/install/bin:\$PATH " >> $SETENV
echo "export LD_LIBRARY_PATH=\$BASE/install/lib:\$LD_LIBRARY_PATH " >> $SETENV

echo Done.
