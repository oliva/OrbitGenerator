#include "OrbitGenerator.h"

#include "TFile.h"
#include "TH2.h"
#include "TH3.h"
#include "TPolyMarker3D.h"
#include "TRandom3.h"

void fluxes_for_trigger_simple(double phi) {
  TRandom3 random;
  const int nctf = 1000;
  const int nz = 28;
  const int A[28][7] = {
    { 1, 2, 0, 0, 0, 0, 0},
    { 2, 4, 0, 0, 0, 0, 0},
    { 6, 7, 0, 0, 0, 0, 0},
    { 7, 9,10, 0, 0, 0, 0},
    {10,11, 0, 0, 0, 0, 0},
    {12,13, 0, 0, 0, 0, 0},
    {14,15, 0, 0, 0, 0, 0},
    {16,17,18, 0, 0, 0, 0},
    {19, 0, 0, 0, 0, 0, 0},
    {20,21,22, 0, 0, 0, 0},
    {23, 0, 0, 0, 0, 0, 0},
    {24,25,26, 0, 0, 0, 0},
    {26,27, 0, 0, 0, 0, 0},
    {28,29,30, 0, 0, 0, 0},
    {31, 0, 0, 0, 0, 0, 0},
    {32,33,34,36, 0, 0, 0},
    {35,36,37, 0, 0, 0, 0},
    {36,37,38,40, 0, 0, 0},
    {39,40,41, 0, 0, 0, 0},
    {41,42,43,44,46,48, 0},
    {45, 0, 0, 0, 0, 0, 0},
    {44,46,47,48,49,50, 0},
    {49,50,51, 0, 0, 0, 0},
    {50,51,52,53,54, 0, 0},
    {53,54,55, 0, 0, 0, 0},
    {54,55,56,57,58,60, 0},
    {57,59, 0, 0, 0, 0, 0},
    {56,58,59,60,61,62,64}
  };
  OrbitGenerator orbit;
  orbit.which_models = 0x3;
  double theta_fov_min = 0; // field of view min theta
  double theta_fov_max = TMath::Pi()/2; // field of view 
  int nr = 500;
  double minr = 0.1; // 100 MV
  double maxr = 10000; // 10 TV
  int nlat = 11;
  double minlat = -55;
  double maxlat = 55;
  int nlon = 18;
  double minlon = -180;
  double maxlon = 180;
  TH3D* flux_vs_logr_vs_lat_vs_lon[nz] = {0};
  for (int iz=0; iz<nz; iz++) {
    int Z = iz+1;
    flux_vs_logr_vs_lat_vs_lon[iz] = new TH3D(Form("flux_vs_logr_vs_lat_vs_lon_Z%02d",Z),
      "; Longitude [deg]; Latitude [deg]; log_{10}(R/GV); Flux [(m^{2} s sr GV)^{-1}]",
      nlon,minlon,maxlon,nlat,minlat,maxlat,nr,log10(minr),log10(maxr)
    ); 
  }
  double alt = 0.5*(408+410); // km 
  for (int ilon=0; ilon<nlon; ilon++) {
    double lon = minlon+(ilon+0.5)*(maxlon-minlon)/nlon; // deg
    for (int ilat=0; ilat<nlat; ilat++) {
      double lat = minlat+(ilat+0.5)*(maxlat-minlat)/nlat; // deg 
      double mlat,mlon,mr;
      int ret = orbit.get_EDGM(lat*M_PI/180,lon*M_PI/180,alt,mlat,mlon,mr,0);
      double rc = orbit.get_Stoermer_cutoff(mr,mlat);
      cout << lon << " " << lat << " " << rc << endl;
      for (int ictf=0; ictf<nctf; ictf++) {
        if (nctf>1) {
          double phi = 2*M_PI*random.Rndm();
          double theta = acos(cos(theta_fov_min)+(cos(theta_fov_max)-cos(theta_fov_min))*random.Rndm());
          double dir[3] = {sin(theta)*cos(phi),sin(theta)*sin(phi),cos(theta)};
          rc = orbit.get_Stoermer_cutoff(mr,mlat,mlon,dir);
        } 
        for (int iz=0; iz<nz; iz++) {
          int Z = iz+1;
          for (int ir=0; ir<nr; ir++) {
            double r = minr*pow(10,(ir+0.5)*(log10(maxr)-log10(minr))/nr);
            for (int iiso=0; iiso<3; iiso++) {
              if (A[iz][iiso]==0) continue; 
              int pdgid = orbit.get_PDGID(Z,A[iz][iiso]);
              double flux = orbit.get_flux(pdgid,r,rc,mr,mlat,phi);
              if ( (flux<=0)||(r<=0) ) continue;
              flux_vs_logr_vs_lat_vs_lon[iz]->Fill(lon,lat,log10(r),flux/nctf); // with weight factor
            }
          }
        }
      }
    }
  }
  // save
  TFile* file = TFile::Open(Form("v1/fluxes_%dMV.root",int(phi*1000)),"recreate");
  for (int iz=0; iz<nz; iz++) flux_vs_logr_vs_lat_vs_lon[iz]->Write();
  file->Close();
  // clean-up
  for (int iz=0; iz<nz; iz++) delete flux_vs_logr_vs_lat_vs_lon[iz]; 
}

void fluxes_for_trigger_simple() {
//  fluxes_for_trigger_simple(0.4);
  fluxes_for_trigger_simple(1.5);
}

