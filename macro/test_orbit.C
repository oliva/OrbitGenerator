#include "OrbitGenerator.h"

#include "TFile.h"
#include "TH3.h"
#include "TPolyMarker3D.h"

void test_orbit(
  unsigned int start_time = 1574331993, // UTC (today)
  int time_interval = 7*24*60*60, // s (1 week) 
  int orbit_type = 3 // (ISS)
) {
  OrbitGenerator orbit;
  // init
  TH3F* axis3D = new TH3F("h3","h3",10,-10000,10000,10,-10000,10000,10,-10000,10000);
  TPolyMarker3D* orbit3D = new TPolyMarker3D(); orbit3D->SetName("orbit3D");
  TGraph* lat_vs_lon = new TGraph(); lat_vs_lon->SetName("lat_vs_lon");
  TGraph* alt_vs_time = new TGraph(); alt_vs_time->SetName("alt_vs_time");
  // loop on seconds 
  for (int it=0; it<time_interval; it+=1) {
    if ((it%3600)==0) cout << "elapsed hours: " << int(it/3600) << " of " << int(time_interval/3600) << endl;
    unsigned int time = start_time + it;
    double lat,lon,alt;
    if      (orbit_type==0) orbit.get_TIANGONG2_simple_orbit(time,lat,lon,alt);
    else if (orbit_type==1) orbit.get_TIANGONG2_predict_orbit(time,lat,lon,alt);
    else if (orbit_type==2) orbit.get_ISS_simple_orbit(time,lat,lon,alt);
    else if (orbit_type==3) orbit.get_ISS_predict_orbit(time,lat,lon,alt);
    // 2D orbit 
    lat_vs_lon->SetPoint(it,lon,lat);
    alt_vs_time->SetPoint(it,it,alt);
    // 3D orbit 
    double R = alt+OrbitGenerator::R_Earth;
    double point[3] = {
      R*cos(lat)*cos(lon),
      R*cos(lat)*sin(lon),
      R*sin(lat),
    };
    orbit3D->SetPoint(it,point[0],point[1],point[2]);
  }
  // save
  TFile* file = TFile::Open("test_orbit.root","recreate");
  axis3D->Write();
  orbit3D->Write();
  lat_vs_lon->Write();
  alt_vs_time->Write();
  file->Close(); 
  // simple draw
  TCanvas* canvas1 = new TCanvas();
  axis3D->Draw();
  orbit3D->Draw();
  canvas1->Update();
  TCanvas* canvas2 = new TCanvas();
  lat_vs_lon->Draw("AP");
  canvas2->Update();
  TCanvas* canvas3 = new TCanvas();
  alt_vs_time->Draw("ALP");
  canvas3->Update();
}

